from junit_xml import TestSuite, TestCase
import json
import requests
import os


status_file = open("status.json")
status = json.load(status_file)
uid = status.get("uid")

TEST_NAME = os.getenv("TEST_NAME", "")
logs_url = f"https://tuxapi.tuxsuite.com/v1/groups/morello/projects/morello-ci/tests/{uid}/logs?format=html"
for test in status.get("tests"):
    result = status.get("results").get(test)
    tc = TestCase(test)
    if result != "pass":
        tc.add_failure_info(message=f"Complete boot logs can be found here: {logs_url}")
    ts = TestSuite(f"{TEST_NAME}", [tc])
print(TestSuite.to_xml_string([ts]))
