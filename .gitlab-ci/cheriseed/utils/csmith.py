#!/usr/bin/env python3
#
# This script builds and runs csmith on two compilers and compares the outputs.
# If the outputs are different, it will indicate that at least one of the
# compilers is incorrect.

import argparse
import difflib
import platform
import shlex
from argparse import Namespace
from pathlib import Path
from typing import Any, List

import utils
from utils import ExecutionInfo, Log, Result, TemporaryResources


class Assets:
    '''Helper to access test assets'''

    def __init__(self, args: Namespace) -> None:
        self.args = args
        self.extension = 'cpp' if args.cpp11 else 'c'

    @property
    def csmith_build_output_path(self) -> Path:
        return self.output_path / 'build'

    @property
    def csmith_install_path(self) -> Path:
        return self.output_path / 'install'

    @property
    def csmith_bin_path(self) -> Path:
        return self.csmith_install_path / 'bin' / 'csmith'

    @property
    def csmith_include_path(self) -> Path:
        return self.csmith_install_path / 'include'

    @property
    def gen_output_path(self) -> Path:
        return self.output_path / 'gen'

    @property
    def bin_output_path(self) -> Path:
        return self.output_path / 'bin'

    @property
    def log_output_path(self) -> Path:
        return self.output_path / 'log'

    @property
    def diff_output_path(self) -> Path:
        return self.output_path / 'diff'

    def gen_file_path(self, idx: int) -> Path:
        return self.gen_output_path / f'gen_{idx}.{self.extension}'

    def bin_file_path(self, idx: int, kind: str) -> Path:
        return self.bin_output_path / f'{idx}.{kind}.bin'

    def log_file_path(self, idx: int, kind: str) -> Path:
        return self.log_output_path / f'{idx}.{kind}.log'

    def diff_file_path(self, idx: int) -> Path:
        return (self.diff_output_path / str(idx)).with_suffix('.diff')

    @property
    def junit_xml_path(self) -> Path:
        return self.output_path / 'csmith_junit_report.xml'

    @property
    def junit_compiler_version_xml_path(self) -> Path:
        return self.output_path / 'compiler_version_junit_report.xml'

    @property
    def junit_file_generation_xml_path(self) -> Path:
        return self.output_path / 'file_generation_junit_report.xml'

    @property
    def junit_compilation_xml_path(self) -> Path:
        return self.output_path / 'compilation_junit_report.xml'

    @property
    def junit_execution_xml_path(self) -> Path:
        return self.output_path / 'execution_junit_report.xml'

    @property
    def junit_compare_xml_path(self) -> Path:
        return self.output_path / 'comparison_junit_report.xml'

    def __getattr__(self, name: str) -> Any:
        return self.args.__getattribute__(name)


class TestFileDescripion:
    def __init__(self, assets: Assets, idx: int, kind: str) -> None:
        self.source_file_path = assets.gen_file_path(idx)
        self.bin_file_path = assets.bin_file_path(idx, kind)
        self.log_file_path = assets.log_file_path(idx, kind)


class CompareFileDescripion:
    def __init__(self, assets: Assets, idx: int):
        self.ref_log_path = assets.log_file_path(idx, 'ref')
        self.exp_log_path = assets.log_file_path(idx, 'exp')
        self.diff_log_path = assets.diff_file_path(idx)


def build_csmith(assets: Assets) -> Result:
    '''Builds csmith in the development environment'''
    Log.pretty().write('Building csmith into ').highlight(
        assets.csmith_build_output_path
    ).flush()

    return utils.execute_commands(
        [
            [
                'cmake',
                '-S',
                str(assets.csmith_path),
                '-B',
                str(assets.csmith_build_output_path),
                '-DCMAKE_BUILD_TYPE=Release',
                '-DCMAKE_C_COMPILER=/usr/bin/gcc',
                '-DCMAKE_CXX_COMPILER=/usr/bin/g++',
                f'-DCMAKE_INSTALL_PREFIX={assets.csmith_install_path}',
                '-DCMAKE_C_FLAGS="-w"',
                '-DCMAKE_CXX_FLAGS="-w"',
                '.',
            ],
            ['make', 'clean'],
            ['make', f'-j{assets.nproc}', 'install'],
        ],
        assets.verbose,
        cwd=assets.csmith_build_output_path,
    )


def query_compiler_versions(assets: Assets) -> Result:
    '''Queries the version of the two compilers'''
    Log.pretty().write('Querying compiler version').flush()

    def get_execution_info(assets: Assets, compiler_path: Path) -> Result:
        return ExecutionInfo(
            name=f'{compiler_path}',
            category='compiler_version',
            command=[str(compiler_path), '--version'],
            check=True,
            timeout=assets.timeout,
            verbose=assets.verbose,
        )

    exec_info_list = [
        get_execution_info(assets, assets.reference_compiler_path),
        get_execution_info(assets, assets.experimental_compiler_path),
    ]
    return utils.execute_parallel(
        assets.nproc,
        exec_info_list,
        testsuite_name=assets.testsuite_name,
        junit_xml_path=assets.junit_compiler_version_xml_path,
        print_summary=False,
    )


def __generation_finished(exec_info: ExecutionInfo) -> ExecutionInfo:
    # csmith generator might take too long. If it timeouts, just try again,
    # it should succeed in a few tries.
    if exec_info.result.is_timeout():
        exec_info.retry = True
    return None


def generate_files(assets: Assets) -> Result:
    Log.pretty().write('Generating files').flush()

    def get_generate_info(
        assets: Assets, csmith_options: List[str], idx: int
    ) -> Result:
        return ExecutionInfo(
            name=f'Generating test file {idx}',
            category=f'generate_{assets.extension}',
            command=[
                str(assets.csmith_bin_path),
                '--output',
                str(assets.gen_file_path(idx)),
                *csmith_options,
            ],
            data=idx,
            check=True,
            timeout=assets.timeout,
            verbose=assets.verbose,
            executed_cb=__generation_finished,
        )

    csmith_options = [
        # Enable some features which are disabled by default
        '--int128',
        '--uint128',
        '--binary-constant',
        # These do not work!
        # '--builtins',
        # '--function-attributes',
        # '--type-attributes',
        # '--label-attributes',
        # '--variable-attributes',
        # '--compiler-attributes',
    ]

    if assets.cpp11:
        csmith_options += ['--lang-cpp', '--cpp11']
    else:
        # This is incompatible with CPP
        csmith_options += ['--float']

    if assets.args.seed:
        csmith_options += ['--seed', str(assets.seed)]

    exec_info_list = [
        get_generate_info(assets, csmith_options, idx + 1)
        for idx in range(assets.iterations)
    ]
    return utils.execute_parallel(
        assets.nproc,
        exec_info_list,
        testsuite_name=assets.testsuite_name,
        junit_xml_path=assets.junit_file_generation_xml_path,
        print_summary=False,
    )


def compile_files(assets: Assets, gen_result: Result) -> Result:
    Log.pretty().write('Compiling generated files').flush()

    def get_compile_info(
        assets: Assets, kind: str, compiler_path: Path, compiler_flags: str, idx: int
    ) -> ExecutionInfo:
        compiler_flags = shlex.split(compiler_flags)

        compiler_flags += ['-I', f'{assets.csmith_include_path}', '-Wno-everything']
        # Disable some errors for CPP.
        if assets.args.cpp11:
            compiler_flags += ['-fpermissive']

        if assets.verbose:
            compiler_flags += ['--verbose']

        data = TestFileDescripion(assets, idx, kind)
        return ExecutionInfo(
            name=f'Compiling {kind} {data.source_file_path.name}',
            category=f'compile_{assets.extension}',
            command=[
                str(compiler_path),
                *compiler_flags,
                str(data.source_file_path),
                '-o',
                str(data.bin_file_path),
            ],
            data=data,
            check=True,
            timeout=assets.timeout,
            verbose=assets.verbose,
        )

    exec_info_list = []
    for gen_exec_info in gen_result.value:
        exec_info_list.append(
            get_compile_info(
                assets,
                'ref',
                assets.reference_compiler_path,
                assets.reference_compiler_flags,
                gen_exec_info.data,
            )
        )
        exec_info_list.append(
            get_compile_info(
                assets,
                'exp',
                assets.experimental_compiler_path,
                assets.experimental_compiler_flags,
                gen_exec_info.data,
            )
        )

    return utils.execute_parallel(
        assets.nproc,
        exec_info_list,
        testsuite_name=assets.testsuite_name,
        junit_xml_path=assets.junit_compilation_xml_path,
        print_summary=False,
    )


def __execution_finished(exec_info: ExecutionInfo) -> ExecutionInfo:
    data: TestFileDescripion = exec_info.data
    with open(data.log_file_path, 'w') as w_file:
        # Timeouts happen because of the code csmith generates.
        # These are fine.
        if exec_info.result.is_timeout():
            w_file.write("execution timed out")
            exec_info.result = Result.skip(exec_info.result.value)
        elif exec_info.stdout is not None:
            w_file.write(exec_info.stdout)

    return None


def execute_binaries(assets: Assets, build_result: Result) -> Result:
    Log.pretty().write('Executing binaries').flush()

    def get_execution_info(assets: Assets, data: TestFileDescripion) -> ExecutionInfo:
        return ExecutionInfo(
            name=f'Executing {data.bin_file_path.name}',
            category=f'execute',
            command=[
                str(data.bin_file_path),
                # print_hash_value
                '1',
            ],
            check=True,
            stderr_to_stdout=True,
            data=data,
            timeout=assets.timeout,
            verbose=assets.verbose,
            executed_cb=__execution_finished,
        )

    exec_info_list = []
    for build_exec_info in build_result.value:
        exec_info_list.append(get_execution_info(assets, build_exec_info.data))

    return utils.execute_parallel(
        assets.nproc,
        exec_info_list,
        testsuite_name=assets.testsuite_name,
        junit_xml_path=assets.junit_execution_xml_path,
        print_summary=False,
    )


def __compare_files(data: Any) -> Result:
    data: CompareFileDescripion = data
    with open(data.ref_log_path, 'r') as r_ref_output:
        with open(data.exp_log_path, 'r') as r_exp_output:
            diff = ''.join(
                difflib.unified_diff(
                    r_ref_output.readlines(),
                    r_exp_output.readlines(),
                    fromfile=str(data.ref_log_path),
                    tofile=str(data.exp_log_path),
                )
            )

    if not diff:
        return Result.success()

    data.diff_log_path.write_text(diff)
    return Result.fail()


def compare_outputs(assets: Assets) -> Result:
    Log.pretty().write('Comparing binary outputs').flush()

    def get_execution_info(assets: Assets, idx: int) -> ExecutionInfo:
        return ExecutionInfo(
            name=f'Comparing case {idx}',
            category='compare',
            command=__compare_files,
            data=CompareFileDescripion(assets, idx),
            timeout=assets.timeout,
            verbose=assets.verbose,
        )

    exec_info_list: List[ExecutionInfo] = []
    for idx in range(assets.iterations):
        exec_info_list.append(get_execution_info(assets, idx + 1))

    return utils.execute_parallel(
        assets.nproc,
        exec_info_list,
        testsuite_name=assets.testsuite_name,
        junit_xml_path=assets.junit_compare_xml_path,
        error_threshold=assets.error_threshold,
    )


def parse_arguments() -> Namespace:
    '''Parses program arguments'''
    parser = argparse.ArgumentParser(
        description='Runs csmith on a reference and an experimental compiler',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '--output-path',
        dest='output_path',
        action='store',
        type=Path,
        default=utils.env_root_path('scratch/csmith'),
        help='output path to store the results',
    )
    parser.add_argument(
        '--csmith-path',
        dest='csmith_path',
        action='store',
        type=Path,
        default=utils.env_root_path('csmith'),
        help='path to csmith source code',
    )
    parser.add_argument(
        '--reference-compiler-path',
        dest='reference_compiler_path',
        action='store',
        type=Path,
        default=None,
        help='path to the reference compiler, selected automatically if unset',
    )
    parser.add_argument(
        '--reference-compiler-flags',
        dest='reference_compiler_flags',
        action='store',
        type=str,
        default='-O2 -static',
        help='flags to the reference compiler',
    )
    parser.add_argument(
        '--experimental-compiler-path',
        dest='experimental_compiler_path',
        action='store',
        type=Path,
        default=utils.env_root_path('llvm-project/build/bin/clang'),
        help='path to the experimental compiler',
    )
    parser.add_argument(
        '--experimental-compiler-flags',
        dest='experimental_compiler_flags',
        action='store',
        type=str,
        default='-O2 -static',
        help='flags to the experimental compiler',
    )
    parser.add_argument(
        '--cpp-11', dest='cpp11', action='store_true', help='generate cpp code'
    )
    mutually_exclusive = parser.add_mutually_exclusive_group()
    mutually_exclusive.add_argument(
        '--iterations',
        dest='iterations',
        action='store',
        default=5,
        type=int,
        help='number of iterations',
    )
    mutually_exclusive.add_argument(
        '--seed',
        dest='seed',
        action='store',
        default=None,
        type=int,
        help='seed to csmith',
    )
    utils.add_usual_arguments(parser, 'csmith', 60.0)

    args = parser.parse_args()
    if args.seed:
        args.iterations = 1
    if not args.reference_compiler_path:
        if args.cpp11:
            args.reference_compiler_path = '/usr/bin/clang++-11'
        else:
            args.reference_compiler_path = '/usr/bin/clang-11'
    return args


def main() -> Result:
    '''The main of the program'''
    assets = Assets(parse_arguments())
    utils.create_empty_directory(assets.output_path)
    # List managed resources.
    resources = [
        assets.csmith_build_output_path,
        assets.csmith_install_path,
        assets.gen_output_path,
        assets.bin_output_path,
        assets.log_output_path,
        assets.diff_output_path,
    ]
    # Clean up before and after tests are executed.
    with TemporaryResources(resources, cleanup=assets.no_cleanup):
        # Build csmith, exit on failure.
        res = utils.try_do(build_csmith, assets)
        if not res.is_success():
            return res
        # Query compiler versions, exit if it failed.
        res = utils.try_do(query_compiler_versions, assets)
        if not res.is_success():
            return res
        gen_res = utils.try_do(generate_files, assets)
        if not gen_res.is_success():
            return gen_res
        build_res = utils.try_do(compile_files, assets, gen_res)
        if not build_res.is_success():
            return build_res
        exec_res = utils.try_do(execute_binaries, assets, build_res)
        if not exec_res.is_success():
            return exec_res
        cmp_res = utils.try_do(compare_outputs, assets, print_details=False)
        return cmp_res


if __name__ == '__main__':
    utils.main_wrapper(main)
