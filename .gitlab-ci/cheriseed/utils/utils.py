#!/usr/bin/env python3
#
# Utilities for better user experience.

import argparse
import contextlib
import json
import os
import re
import shutil
import socket
import subprocess
import sys
import textwrap
import time
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from io import StringIO
from multiprocessing import Pool
from pathlib import Path
from typing import Any, Callable, List, Optional, Union
from xml.etree.ElementTree import Element, ElementTree, SubElement


def env_root_path(resource_path: Union[str, Path]) -> Path:
    '''Returns the proper path to a file in the development environment'''
    return Path('/') / resource_path


def create_empty_directory(desired_path: Path) -> None:
    '''Makes sure a desired path exists and is empty'''
    Log.pretty().write('Preparing empty directory at ').highlight(desired_path).flush(
        end=''
    )
    shutil.rmtree(desired_path, ignore_errors=True)
    desired_path.mkdir(parents=True)
    Log.success()


class AnsiColors:
    '''ANSI color codes'''


def __generate_ansi_colors_class() -> None:
    '''Generates the rest of AnsiColors class based on available colors.'''

    class Colors(Enum):
        RED = 31
        GREEN = 32
        YELLOW = 33
        BLUE = 34
        MAGENTA = 35
        CYAN = 36
        WHITE = 37

    if sys.stdin.isatty() and sys.stdout.isatty():
        code = lambda value: f'\033[{value}m'
    else:
        code = lambda value: ''

    setattr(AnsiColors, 'RESET_ALL', code('0'))
    for color in Colors:
        setattr(AnsiColors, color.name, code(f'{color.value}'))
        setattr(AnsiColors, f'{color.name}_B', code(f'1;{color.value}'))
        setattr(
            AnsiColors, f'{color.name}_INV', code('30') + code(f'{color.value + 10}')
        )
        setattr(
            AnsiColors,
            f'{color.name}_INV_B',
            code('30') + code(f'1;{color.value + 10}'),
        )


#  Trigger code generation when module gets loaded.
__generate_ansi_colors_class()


class ResultCode(Enum):
    '''Possible result codes of an operation'''

    SUCCESS = 0
    FAIL = 1
    TIMEOUT = 2
    SKIP = 3
    INTERRUPTED = 4
    XFAIL = 5


class ResultCodeColor(Enum):
    '''Colors associated with ResultCode values'''

    SUCCESS = 'GREEN'
    FAIL = 'RED'
    TIMEOUT = 'MAGENTA'
    SKIP = 'YELLOW'
    INTERRUPTED = 'CYAN'
    XFAIL = 'YELLOW_INV'

    def as_color(self) -> AnsiColors:
        return getattr(AnsiColors, self.value)

    def as_color_inverted(self) -> AnsiColors:
        return getattr(AnsiColors, f'{self.value}_INV')

    def as_bold_color(self) -> AnsiColors:
        return getattr(AnsiColors, f'{self.value}_B')

    def as_bold_color_inverted(self) -> AnsiColors:
        return getattr(AnsiColors, f'{self.value}_INV_B')


class Remarks:
    '''Class to collect remarks during test execution'''

    def __init__(self) -> None:
        self.__remarks: List[str] = []
        self.__on_exit: List[Any] = []

    def append(self, message: str) -> None:
        self.__remarks.append(message)

    def on_exit(self, obj: Any) -> None:
        self.__on_exit.append(obj)

    def __enter__(self) -> 'Remarks':
        return self

    def __exit__(self, type, value, traceback) -> bool:
        for obj in self.__on_exit:
            obj.last_remark(self)

        if not self.__remarks:
            return False

        Log.pretty().write('REMARKS\n').flush()
        for res in self.__remarks:
            Log.write(res)

        Log.write('')
        return False


class TemporaryResources:
    '''Context manager class which removes paths when destroyed'''

    @dataclass
    class File:
        path: Path

        def __str__(self) -> str:
            return str(self.path)

    @dataclass
    class Directory:
        path: Path
        src_path: Path

        def __str__(self) -> str:
            return str(self.path)

    def __init__(self, resources: List[Any], cleanup) -> None:
        self.resources = resources
        self.cleanup = cleanup

    def __enter__(self) -> 'TemporaryResources':
        for res in self.resources:
            if isinstance(res, Path):
                shutil.rmtree(res, ignore_errors=True)
                res.mkdir(parents=True)
            elif isinstance(res, TemporaryResources.File):
                res.path.unlink(missing_ok=True)
            elif isinstance(res, TemporaryResources.Directory):
                shutil.rmtree(res.path, ignore_errors=True)
                shutil.copytree(res.src_path, res.path)
            else:
                Log.warning(f'  Don\'t know how to create {res}')
        return self

    def __exit__(self, type, value, traceback) -> bool:
        if not self.cleanup:
            return False
        if self.resources:
            Log.pretty().write('CLEANUP\n').flush()
        for res in self.resources:
            Log.write(f'  Removing {res}')
            if isinstance(res, Path):
                shutil.rmtree(res, ignore_errors=True)
            elif isinstance(res, TemporaryResources.File):
                res.path.unlink(missing_ok=True)
            elif isinstance(res, TemporaryResources.Directory):
                shutil.rmtree(res.path, ignore_errors=True)
            else:
                Log.warning(f'  Don\'t know how to release {res}')

        return False


class ElapsedTime:
    '''Class to measure execution time'''

    def __init__(self) -> None:
        self.start_time = time.perf_counter()
        self.elapsed_time = None

    def set_elapsed_time(self, value: Optional[float] = None) -> None:
        self.elapsed_time = value

    def elapsed_time_s(self) -> str:
        if self.elapsed_time is None:
            self.elapsed_time = time.perf_counter() - self.start_time
        return '{:.2f}'.format(self.elapsed_time)


class Log:
    '''Prints to stdout with or without coloring'''

    class LogPretty:
        '''Helper class to print with two colors'''

        def __init__(self, base_color: str, color: str) -> None:
            self.base_color = base_color
            self.color = color
            self.msg = []

        def append(self, msg: str) -> 'Log':
            self.msg.append(msg)
            return self

        def write(self, msg: str, color=None) -> 'Log':
            self.msg.append(Log.format(color if color else self.base_color, msg))
            return self

        def highlight(self, msg: str) -> 'Log':
            self.msg.append(Log.format(self.color, msg))
            return self

        def to_string(self) -> str:
            return ''.join(self.msg)

        def flush(self, end: str = '\n') -> 'Log':
            print(self.to_string(), end=end)
            sys.stdout.flush()
            self.msg = []
            return self

    def pretty() -> 'Log.LogPretty':
        return Log.LogPretty(AnsiColors.CYAN, AnsiColors.WHITE_B)

    @classmethod
    def print(cls, color: str, msg: str, end: str = '\n') -> None:
        print(cls.format(color, msg), end=end)
        sys.stdout.flush()

    @classmethod
    def format(cls, color: str, msg: str) -> str:
        return f'{color}{msg}{AnsiColors.RESET_ALL}'

    @classmethod
    def write(cls, msg: str, end: str = '\n') -> None:
        cls.print(AnsiColors.WHITE, msg, end=end)

    @classmethod
    def highlight(cls, msg: str, end: str = '\n') -> None:
        cls.print(AnsiColors.CYAN_B, msg, end=end)

    @classmethod
    def warning(cls, msg: str, end: str = '\n') -> None:
        cls.print(AnsiColors.YELLOW, msg, end=end)


def __generate_log_class() -> None:
    '''Generates the rest of Log class based on available ResultCodeColor values.

    Generated members:

        @classmethod
        def <code.name.lower()>(cls, msg: str=None) -> None
    '''

    def log_wrapper(
        color_name: str, code_color: ResultCodeColor
    ) -> Callable[[Log, Optional[str]], None]:
        def log(cls, msg: Optional[str] = None) -> None:
            if msg:
                cls.print(code_color.as_color(), msg)
            else:
                cls.write(' ', end='')
                cls.print(code_color.as_bold_color(), f'[{color_name.upper()}]')

        setattr(Log, color_name.lower(), classmethod(log))

    # Colors could be duplicated
    for (color_name, code_color) in ResultCodeColor.__members__.items():
        log_wrapper(color_name, code_color)


#  Trigger code generation when module gets loaded.
__generate_log_class()


@dataclass
class Result:
    '''Result an operation: a result code and an optional value'''

    code: ResultCode = ResultCode.FAIL
    value: Optional[Any] = None

    def print(self) -> None:
        getattr(Log, self.code.name.lower())()

    def __str__(self) -> str:
        return self.code.name


def __generate_result_class() -> None:
    '''Generates the rest of Result class based on available ResultCode values.

    Generated members:

        @staticmethod
        def <code.name.lower()>(value: Any=None) -> Result

        def is_<code.name.lower()>(self) -> bool
    '''

    def builder_wrapper(code: ResultCode) -> Callable[[Optional[Any]], Result]:
        def builder(value: Optional[Any] = None) -> Result:
            if isinstance(value, Result):
                value = value.value
            return Result(code=code, value=value)

        return builder

    def compare_wrapper(code: ResultCode):
        def compare(self) -> bool:
            return self.code == code

        return compare

    for code in ResultCode:
        setattr(Result, code.name.lower(), staticmethod(builder_wrapper(code)))
        setattr(Result, f'is_{code.name.lower()}', compare_wrapper(code))


#  Trigger code generation when module gets loaded.
__generate_result_class()


@dataclass
class ExecutionInfo:
    '''Data class to hold common execution info'''

    name: str
    command: List[str]
    category: str = ''
    timeout: float = 600.0
    env: Optional[dict] = None
    cwd: Optional[Path] = None
    check: bool = True
    data: Optional[Any] = None
    stdin: Optional[str] = None
    verbose: bool = False
    stderr_to_stdout: bool = False
    # Called when execution completed - whatever the result may be.
    executed_cb: Optional[Any] = None
    retry: bool = False
    # Filled-in after execution
    stdout: Optional[str] = None
    stderr: Optional[str] = None
    returncode: int = -1
    result: Result = Result.fail()
    elapsed_time: float = 0.0

    def __lt__(self, other) -> bool:
        return f'{self.category}.{self.name}' < f'{other.category}.{other.name}'


@dataclass
class ExpectedFailure:
    '''Data class to hold information about an expected failure'''

    name: str
    category: List[str]
    hit: bool = False


class ExpectedFailures:
    '''Class which holds expected failures'''

    def __init__(
        self, expected_failures: List[ExpectedFailure], remarks: Remarks
    ) -> None:
        self.expected_failures = expected_failures
        self.remarks = remarks
        self.remarks.on_exit(self)

    @staticmethod
    def parse_from_file(
        expected_failures_path: Path, remarks: Remarks
    ) -> 'ExpectedFailures':
        if expected_failures_path is None:
            return ExpectedFailures([], remarks)

        expected_failures = []
        with open(expected_failures_path) as r_file:
            entries = json.load(r_file)

        for entry in entries:
            expected_failures.append(
                ExpectedFailure(entry.get('name'), entry.get('category'))
            )

        return ExpectedFailures(expected_failures, remarks)

    def check(self, exec_info: ExecutionInfo) -> ExecutionInfo:
        expected_failure = None
        for failure in self.expected_failures:
            if not re.fullmatch(failure.name, exec_info.name):
                continue
            if not re.fullmatch(failure.category, exec_info.category):
                continue
            expected_failure = failure

        if expected_failure is None:
            return exec_info

        # Mark this entry as used.
        expected_failure.hit = True

        if exec_info.result.is_success():
            self.remarks.append(
                Log.LogPretty(AnsiColors.WHITE, AnsiColors.CYAN)
                .highlight(exec_info.category.upper())
                .write(' ')
                .write(exec_info.name, color=AnsiColors.WHITE_B)
                .write(' is ')
                .write('expected to fail', color=ResultCodeColor.XFAIL.as_bold_color())
                .write(' but it was ')
                .write('successful', color=ResultCodeColor.SUCCESS.as_bold_color())
                .write('.')
                .to_string()
            )
            return exec_info

        exec_info.result = Result.xfail(value=exec_info.result)
        return exec_info

    def last_remark(self, remarks: Remarks) -> None:
        for failure in self.expected_failures:
            if failure.hit:
                continue
            remarks.append(
                Log.LogPretty(AnsiColors.WHITE, AnsiColors.CYAN)
                .write('Expected failure ')
                .highlight(failure.category.upper())
                .write(' ')
                .write(failure.name, color=AnsiColors.WHITE_B)
                .write(' was unused.')
                .to_string()
            )


class ExecutionSummary:
    '''Holds executive summary of execution'''

    def __init__(self, name: str) -> None:
        self.name = name
        # Number of tolerated failures
        self.error_threshold: int = 0
        #  Generate members
        for code in ResultCode:
            member_name = f'{code.name.lower()}_list'
            setattr(self, member_name, list())

    @property
    def error_count(self) -> int:
        return self.timeout_count + self.fail_count

    def append(self, exec_info: ExecutionInfo) -> None:
        getattr(self, f'{exec_info.result.code.name.lower()}_list').append(exec_info)

    def summarize(
        self,
        junit_xml_path: Path = None,
        duration: str = '0',
        allow_unstable: bool = False,
        print_summary: bool = True,
    ) -> Result:
        if print_summary:
            self.__summarize_results()

        if junit_xml_path:
            self.__emit_junit_xml(junit_xml_path, duration)

        if allow_unstable:
            Log.warning(f'REMINDER: --allow-unstable is set')
            return Result.success(value=self.all_results)

        if self.error_threshold != 0:
            Log.warning(f'REMINDER: Error threshold is set to {self.error_threshold}.')

        if self.error_count <= self.error_threshold:
            return Result.success(value=self.all_results)

        return Result.fail(value=self.all_results)

    def __summarize_results(self) -> None:
        '''Summarizes results'''

        Log.print(AnsiColors.CYAN, '\nSUMMARY\n')
        Log.print(
            AnsiColors.WHITE_B, f' {"TOTAL:".ljust(8)}  {self.test_count:>4}        '
        )

        if self.test_count == 0:
            Log.warning('No tests was run')
            return

        for code in ResultCode:
            code_name_lower = code.name.lower()
            count = getattr(self, f'{code_name_lower}_count')
            if count > 0:
                code_name_upper = code_name_lower.upper()
                ratio = getattr(self, f'{code_name_lower}_ratio')
                line = (
                    f' {(code_name_upper + ":").ljust(8)}  {count:>4} [{ratio:>4.0%}] '
                )
                color = getattr(ResultCodeColor, code_name_upper).as_color()
                Log.print(color, line)

        Log.write('')

    def __emit_junit_xml(self, junit_xml_path: Path, duration: str) -> None:
        tree = ElementTree(Element('testsuites'))
        # <testsuites>
        testsuites = tree.getroot()
        testsuites.tail = '\n'
        testsuites.text = '\n'
        # <testsuite ... >
        testsuite = SubElement(testsuites, 'testsuite')
        testsuite.set('name', self.name)
        testsuite.set('package', '')
        testsuite.set('timestamp', str(datetime.utcnow().isoformat()))
        testsuite.set('hostname', socket.getfqdn())
        testsuite.set('tests', str(self.test_count))
        testsuite.set('failures', str(self.error_count))
        testsuite.set('errors', str(0))
        testsuite.set('skipped', str(self.skip_count))
        testsuite.set('time', str(duration))
        testsuite.tail = '\n'
        testsuite.text = '\n'
        self.__junit_testcases(testsuite, self.all_results)
        with open(junit_xml_path, 'wb') as w_file:
            tree.write(w_file, encoding='utf-8', xml_declaration=True)

    def __junit_testcases(
        self, testsuite: Element, testcases: List[ExecutionInfo]
    ) -> None:
        re_ansi_colors = re.compile('\033\[.*?m')

        def sanitize_field(field) -> str:
            return field.replace('-', '.').replace('/', '.')

        for testcase in sorted(testcases):
            # <testcase name="..." classname="..." time="...">
            tc = SubElement(testsuite, 'testcase')
            tc.set('name', testcase.name)
            tc.set('classname', f'{self.name}.{sanitize_field(testcase.category)}')
            tc.set('time', str(testcase.elapsed_time))
            tc.tail = '\n'
            tc.text = '\n'
            if testcase.result.is_success() or testcase.result.is_xfail():
                continue
            if testcase.result.is_skip():
                # <skipped />
                skipped = SubElement(tc, 'skipped')
                skipped.tail = '\n'
                continue
            # <failure message="..." type="...">
            failure = SubElement(tc, 'failure')
            failure.tail = '\n'
            sanitized_message = re_ansi_colors.sub('', testcase.stderr)
            failure.set('message', sanitized_message)
            failure.set('type', 'runtime error')


def __generate_execution_summary_class():
    '''Generates the rest of ExecutionSummary class based on available ResultCode values.

    Generated members:
        def <code.name.lower()>_list(self) -> List[ExecutionInfo]

        def <code.name.lower()>_count(self) -> int

        def <code.name.lower()>_ratio(self) -> float

        def test_count(self) -> int

        def all_results(self) -> List[ExecutionInfo]
    '''

    count_names = []
    member_names = []
    for code in ResultCode:
        count_name = f'{code.name.lower()}_count'
        count_names.append(count_name)
        member_name = f'{code.name.lower()}_list'
        member_names.append(member_name)

        def count_name_wrapper(member_name: str) -> Callable[[ExecutionSummary], int]:
            def count_name(self) -> int:
                return len(getattr(self, member_name))

            return count_name

        def ratio_wrapper(count_name: str) -> Callable[[ExecutionSummary], float]:
            def ratio(self) -> float:
                return getattr(self, count_name) / self.test_count

            return ratio

        setattr(ExecutionSummary, count_name, property(count_name_wrapper(member_name)))
        setattr(
            ExecutionSummary,
            f'{code.name.lower()}_ratio',
            property(ratio_wrapper(count_name)),
        )

    def test_count(self) -> int:
        count: int = 0
        for count_name in count_names:
            count += getattr(self, count_name)
        return count

    def all_results(self) -> List[ExecutionSummary]:
        results: List[ExecutionInfo] = list()
        for member_name in member_names:
            results.extend(getattr(self, member_name))
        return results

    setattr(ExecutionSummary, 'test_count', property(test_count))
    setattr(ExecutionSummary, 'all_results', property(all_results))


#  Trigger code generation when module gets loaded.
__generate_execution_summary_class()


def print_exec_info(exec_info: ExecutionInfo) -> None:
    '''Prints a command and it's output in a nice way'''
    if callable(exec_info.command):
        command = exec_info.command.__name__
    else:
        command = ' '.join(exec_info.command)

    lines = '\n'.join(
        [
            Log.format(AnsiColors.BLUE, '[command]'),
            Log.format(AnsiColors.WHITE, command),
            Log.format(AnsiColors.BLUE, '[stdout]'),
            Log.format(AnsiColors.WHITE, exec_info.stdout),
            Log.format(AnsiColors.BLUE, '[stderr]'),
            Log.format(AnsiColors.WHITE, exec_info.stderr),
        ]
    )
    indent = ' ' * 4
    Log.write(textwrap.indent(lines, indent))


def try_do(function, *args, print_details: bool = True, **kwargs) -> Result:
    '''Tries to run an operation and prints results in an uniform way'''
    et = ElapsedTime()
    try:
        res = function(*args, **kwargs)
    except subprocess.TimeoutExpired as ex:
        res = Result.timeout()
        et.set_elapsed_time(ex.timeout)
    except subprocess.CalledProcessError as ex:
        res = Result.fail()
    except Exception as ex:
        Log.fail()
        res = Result.fail()

    if not print_details:
        return res

    Log.print(AnsiColors.WHITE, f' [{et.elapsed_time_s()} s]', '')
    res.print()
    return res


def execute_commands(
    commands: list, verbose: bool, timeout: float = 60.0, cwd=None
) -> None:
    '''Executes a list of commands'''
    for command in commands:
        if verbose:
            msg = ' '.join(command) if isinstance(command, list) else command
            Log.pretty().write('Executing ').highlight(msg).flush()
        subprocess.run(command, check=True, timeout=timeout, cwd=cwd)
    return Result.success()


def __execute_function_with_info(exec_info: ExecutionInfo) -> Result:
    '''Executes a command as a function and translates the outcome into a Result'''
    et = ElapsedTime()
    try:
        with contextlib.redirect_stdout(StringIO()) as proc_stdout:
            with contextlib.redirect_stdout(StringIO()) as proc_stderr:
                exec_info.result = exec_info.command(exec_info.data)
    except Exception as ex:
        exec_info.result = Result.fail()
        exec_info.stderr = str(ex)
    else:
        exec_info.stdout = proc_stdout.getvalue()
        exec_info.stderr = proc_stderr.getvalue()
    finally:
        exec_info.elapsed_time = et.elapsed_time_s()


def __execute_subprocess_with_info(exec_info: ExecutionInfo) -> None:
    '''Executes a command as a subprocess and translates the outcome into a Result'''
    et = ElapsedTime()
    try:
        process = subprocess.run(
            exec_info.command,
            check=exec_info.check,
            timeout=exec_info.timeout,
            input=exec_info.stdin,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT if exec_info.stderr_to_stdout else subprocess.PIPE,
            env=exec_info.env if exec_info.env else os.environ.copy(),
            cwd=exec_info.cwd,
            universal_newlines=True,
        )
    except KeyboardInterrupt:
        exec_info.result = Result.interrupted()
    except subprocess.TimeoutExpired as ex:
        exec_info.result = Result.timeout()
        exec_info.stderr = ex.output.decode('utf-8') if ex.output else str(ex)
        et.set_elapsed_time(ex.timeout)
    except subprocess.CalledProcessError as ex:
        exec_info.result = Result.fail()
        exec_info.stdout = ex.stdout.strip()
        if ex.stderr is not None:
            exec_info.stderr = ex.stderr.strip()
    except Exception as ex:
        exec_info.result = Result.fail()
        exec_info.stderr = str(ex)
    else:
        exec_info.result = Result.success()
        exec_info.stdout = process.stdout.strip()
        if process.stderr is not None:
            exec_info.stderr = process.stderr.strip()
        exec_info.returncode = process.returncode

    exec_info.elapsed_time = et.elapsed_time_s()


def __execute_with_info(exec_info: ExecutionInfo) -> None:
    '''Executes a command and translates the outcome into a Result'''
    if callable(exec_info.command):
        __execute_function_with_info(exec_info)
    else:
        __execute_subprocess_with_info(exec_info)


def _execute(exec_info: ExecutionInfo) -> ExecutionInfo:
    '''Used by execute_parallel() to execute a command'''
    if exec_info.result and exec_info.result.is_skip():
        return exec_info
    __execute_with_info(exec_info)
    return exec_info


class ParallelExecution:
    '''Class to execute tasks in parallel using a process pool'''

    def __init__(
        self,
        processes: int,
        testsuite_name: str = 'test',
        expected_failures: Optional[ExpectedFailures] = None,
        error_threshold: int = 0,
    ):
        # Overall elapsed time
        self._duration = ElapsedTime()
        self._exec_summary = ExecutionSummary(name=testsuite_name)
        self._exec_summary.error_threshold = error_threshold
        self._pool = Pool(processes)
        self._expected_failures = expected_failures

    def execute(self, exec_info_list: List[ExecutionInfo]) -> None:
        if not exec_info_list:
            return

        name_length: int = max([len(info.name) for info in exec_info_list])
        name_length = max(name_length + 4, 24)

        category_length: int = max([len(info.category) for info in exec_info_list])
        category_length = max(category_length + 4, 16)

        worklist = exec_info_list
        new_worklist = []
        while worklist:
            for exec_info in self._pool.imap_unordered(_execute, worklist):
                if exec_info.executed_cb:
                    new_work_item = exec_info.executed_cb(exec_info)
                    if new_work_item:
                        new_worklist.append(new_work_item)
                    if exec_info.retry:
                        new_worklist.append(exec_info)
                        exec_info.retry = False
                        continue

                Log.pretty().write(
                    exec_info.category.upper().ljust(category_length)
                ).highlight(exec_info.name.ljust(name_length)).flush(end='')

                # Check if there is any expected failure.
                if self._expected_failures:
                    exec_info = self._expected_failures.check(exec_info)
                self._exec_summary.append(exec_info)

                # Print result
                exec_info.result.print()
                if exec_info.verbose or exec_info.stderr:
                    print_exec_info(exec_info)
                elif exec_info.result.is_success() or exec_info.result.is_skip():
                    pass
                else:
                    print_exec_info(exec_info)

            worklist = new_worklist
            new_worklist = []

    def summarize(
        self,
        junit_xml_path: Optional[Path] = None,
        allow_unstable: bool = False,
        print_summary: bool = True,
    ) -> Result:
        return self._exec_summary.summarize(
            junit_xml_path=junit_xml_path,
            duration=self._duration.elapsed_time_s(),
            allow_unstable=allow_unstable,
            print_summary=print_summary,
        )


def execute_parallel(
    processes: int,
    exec_info_list: List[ExecutionInfo],
    error_threshold: int = 0,
    testsuite_name: str = 'test',
    junit_xml_path: Optional[Path] = None,
    allow_unstable: bool = False,
    print_summary: bool = True,
    expected_failures: Optional[ExpectedFailures] = None,
) -> Result:
    '''Executes a list of commands with a process pool and prints the results'''
    pool = ParallelExecution(
        processes,
        testsuite_name=testsuite_name,
        expected_failures=expected_failures,
        error_threshold=error_threshold,
    )
    pool.execute(exec_info_list)
    return pool.summarize(
        junit_xml_path=junit_xml_path,
        allow_unstable=allow_unstable,
        print_summary=print_summary,
    )


def add_usual_arguments(
    parser: argparse.ArgumentParser, testsuite_name: str, timeout: float
) -> None:
    '''Extends an argument parser with usual arguments.'''
    parser.add_argument(
        '--testsuite-name',
        dest='testsuite_name',
        action='store',
        type=str,
        default=testsuite_name,
        help='name of the test suite; produces a JUnit xml summary',
    )
    parser.add_argument(
        '--allow-unstable',
        dest='allow_unstable',
        action='store_true',
        help='exit with 0 even if tests failed',
    )
    parser.add_argument(
        '--error-threshold',
        dest='error_threshold',
        action='store',
        type=int,
        default=0,
        help='number of tolerated errors, including failures and timeouts',
    )
    parser.add_argument(
        '--expected-failures-path',
        dest='expected_failures_path',
        action='store',
        type=str,
        default=None,
        help='path to a JSON file which describes expected failures',
    )
    parser.add_argument(
        '--timeout',
        dest='timeout',
        action='store',
        default=timeout,
        type=float,
        help='timeout for commands',
    )
    parser.add_argument(
        '--nproc',
        dest='nproc',
        action='store',
        type=int,
        default=os.cpu_count(),
        help='number of parallel processes',
    )
    parser.add_argument(
        '--no-cleanup',
        dest='no_cleanup',
        action='store_false',
        help='do not cleanup test resources before exit',
    )
    parser.add_argument(
        '--verbose', dest='verbose', action='store_true', help='enable verbose output'
    )


def main_wrapper(main) -> None:
    '''Wraps main to ensure the exit code is properly set'''
    if main().is_success():
        sys.exit(0)
    sys.exit(1)
