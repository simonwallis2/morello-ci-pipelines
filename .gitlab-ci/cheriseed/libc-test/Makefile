# ------------------------------------------------------------------------------
# Configurable options

Q?=@
CC?=/llvm-project/build/bin/clang
AR?=/llvm-project/build/bin/llvm-ar
RANLIB?=/llvm-project/build/bin/llvm-ranlib
TARGET_TRIPLE?=x86_64-linux-musl
LIBC_TEST_PATH?=/libc-test
LIBC_TEST_OUTPUT_PATH?=/scratch/libc-test
MUSL_LIBC_SYSROOT_PATH?=/musl-libc/obj/install/$(TARGET_TRIPLE)
BIN?=.bin
EXTRA_C_FLAGS?=

# ------------------------------------------------------------------------------
# Not configurable options

arch:=$(shell TT=$(TARGET_TRIPLE); echo "$${TT%%-*}")
src_path:=$(LIBC_TEST_PATH)/src
output_path=$(LIBC_TEST_OUTPUT_PATH)
options.h:=$(output_path)/common/options.h
libtest.a:=$(output_path)/common/libtest.a
static.bin=.static$(BIN)
dynamic.bin=.dynamic$(BIN)

includes:= \
	-I $(src_path)/common \
	-I $(output_path)/common

c_flags:= \
	-fuse-ld=lld \
	--target=$(TARGET_TRIPLE) \
	--sysroot=$(MUSL_LIBC_SYSROOT_PATH) \
	--rtlib=compiler-rt \
	-pipe \
	-g \
	-std=c99 \
	-D_POSIX_C_SOURCE=200809L \
	-fno-builtin \
	-Wall \
	-Werror=implicit-function-declaration \
	-Werror=implicit-int \
	-Werror=pointer-sign \
	-Werror=pointer-arith \
	-Wno-error \
	-Wno-unused-function \
	-Wno-unused-variable \
	-Wno-unused-command-line-argument \
	-Wno-unknown-pragmas \
	-Wno-missing-braces \
	-Wno-string-plus-int \
	-Wno-literal-range \
	-Wno-ignored-pragmas \
	-Wno-incompatible-pointer-types \
	$(EXTRA_C_FLAGS)

api.objects.c_flags:= \
	-pedantic-errors \
	-Werror \
	-Wno-unused \
	-Wno-switch-bool \
	-D_XOPEN_SOURCE=700

ld_flags:= \
	-lc \
	-lpthread \
	-lm \
	-lrt \
	-ldl

# ------------------------------------------------------------------------------
# Helper macros

# Finds all sources in a given directory
# Arguments:
#   1: Name of the sub-directory, e.g. 'common'
define find_sources
$(sort $(subst $(src_path)/,,$(wildcard $(src_path)/$(1)/*.c)))
endef

# Builds a static binary from a single source file
# Arguments:
#   1: Target name
#   2: Sources to build
#   3: Variable to append the target to
define build_static_binary
$(3)+= $(1)
$(1): $(2)
	$(Q)$(CC) $(includes) $(c_flags) -static $$? $(ld_flags) -o $$@
endef

# Builds a static binary from a single source file
# Arguments:
#   1: Relative "name" of the source file, e.g. 'common/runtest.c'
#   2: Variable to append the target to
define static_binary
$(eval $(call build_static_binary,$(output_path)/$(1:%.c=%$(static.bin)),$(src_path)/$(1) $(libtest.a),$(2)))
endef

# Builds a dynamic binary from a single source file
# Arguments:
#   1: Target name
#   2: Sources to build
#   3: Variable to append the target to
define build_dynamic_binary
$(3)+= $(1)
$(1): $(2)
	$(Q)$(CC) $(includes) $(c_flags) -shared-libsan -Wl,--dynamic-linker=$(MUSL_LIBC_SYSROOT_PATH)/lib/libc.so $$? $(ld_flags) -o $$@
endef

# Builds a dynamic binary from a single source file
# Arguments:
#   1: Relative "name" of the source file, e.g. 'common/runtest.c'
#   2: Variable to append the target to
define dynamic_binary
$(eval $(call build_dynamic_binary,$(output_path)/$(1:%.c=%$(dynamic.bin)),$(src_path)/$(1) $(libtest.a),$(2)))
endef

# Builds an object file from single source file
# Arguments:
#   1: Target name
#   2: Sources to build
#   3: Variable to append the target to
define build_object_file
$(3)+= $(1)
$(1): $(2) $(options.h)
	$(Q)$(CC) $(includes) $(c_flags) $($(3).c_flags) -c $$< -o $$@
endef

# Builds a object file from a single source file
# Arguments:
#   1: Relative "name" of the source file, e.g. 'api/aio.c'
#   2: Variable to append the target to
define object_file
$(eval $(call build_object_file,$(output_path)/$(1:%.c=%.o),$(src_path)/$(1),$(2)))
endef

# ------------------------------------------------------------------------------
# Creates the output directory

$(output_path):
	$(Q)rm -rf $(output_path)
	$(Q)mkdir -p \
		$(output_path)/api \
		$(output_path)/common \
		$(output_path)/functional \
		$(output_path)/math \
		$(output_path)/regression

# ------------------------------------------------------------------------------
# Creates 'options.h' as defined in 'options.h.in'

$(options.h): $(output_path)
	$(Q)$(CC) -E - \
		< $(src_path)/common/options.h.in \
		> $(options.h)
	$(Q)sed \
		-e '1,/optiongroups_unistd_end/d' \
		-e '/^#/d' \
		-e '/^[[:space:]]*$$/d' \
		-e 's/^/#define /' \
		-i $(options.h)

# ------------------------------------------------------------------------------
# Builds 'libtest.a' archive

$(foreach src,$(call find_sources,common),$(eval $(call object_file,$(src),libtest.objects)))

$(libtest.a): $(libtest.objects)
	$(Q)$(AR) rc $(libtest.a) $^
	$(Q)$(RANLIB) $(libtest.a)

# ------------------------------------------------------------------------------
# Builds 'runtest' binary

$(eval $(call static_binary,common/runtest.c,binaries.static))
$(eval $(call dynamic_binary,common/runtest.c,binaries.dynamic))

# ------------------------------------------------------------------------------
# Builds 'api' tests

# 'src/api/unistd.c' does not compile with musl-libc.
api.sources:=$(filter-out \
	%/unistd.c, \
	$(call find_sources,api) \
)
$(foreach src,$(api.sources),$(eval $(call object_file,$(src),api.objects)))
$(eval $(call build_static_binary,$(output_path)/api/main$(static.bin),$(api.objects),binaries.static))
$(eval $(call build_dynamic_binary,$(output_path)/api/main$(dynamic.bin),$(api.objects),binaries.dynamic))

# ------------------------------------------------------------------------------
# Builds all 'functional' tests

functional.sources:=$(filter-out \
	%dso.c %/dlopen.c %/tls_align.c %/tls_align_dlopen.c %/tls_init_dlopen.c,\
	$(call find_sources,functional) \
)
$(foreach src,$(functional.sources),$(eval $(call static_binary,$(src),binaries.static)))
$(foreach src,$(functional.sources),$(eval $(call dynamic_binary,$(src),binaries.dynamic)))

# ------------------------------------------------------------------------------
# Builds all 'math' tests

math.sources:=$(call find_sources,math)
$(foreach src,$(math.sources),$(eval $(call static_binary,$(src),binaries.static)))
$(foreach src,$(math.sources),$(eval $(call dynamic_binary,$(src),binaries.dynamic)))

# ------------------------------------------------------------------------------
# Builds all 'regression' tests

regression.sources:=$(filter-out \
	%dso.c %/tls_get_new-dtv.c, \
	$(call find_sources,regression) \
)
$(foreach src,$(regression.sources),$(eval $(call static_binary,$(src),binaries.static)))
$(foreach src,$(regression.sources),$(eval $(call dynamic_binary,$(src),binaries.dynamic)))

# ------------------------------------------------------------------------------
# Targets

all: static dynamic

static: $(binaries.static)

dynamic: $(binaries.dynamic)

clean:
	$(Q)rm -rf $(output_path)

# ------------------------------------------------------------------------------
