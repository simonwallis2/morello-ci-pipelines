#!/usr/bin/bash

set -e
set -x

# shellcheck source=setup-cheriseed-build-env.sh
. "${CI_PROJECT_DIR}"/.gitlab-ci/build/setup-cheriseed-build-env.sh

set -u

# Source directories.
readonly LLVM_PROJECT_PATH="${CI_PROJECT_DIR}/llvm-project"
readonly MUSL_LIBC_PATH="${CI_PROJECT_DIR}/musl-libc"
readonly LIBSHIM_PATH="${CI_PROJECT_DIR}/libshim"
readonly LIBC_TEST_PATH="${CI_PROJECT_DIR}/libc-test"
readonly LLVM_TEST_SUITE_PATH="${CI_PROJECT_DIR}/llvm-test-suite"
readonly CSMITH_PATH="${CI_PROJECT_DIR}/csmith"

# Further dependencies.
readonly LIBC_TEST_URL="git://repo.or.cz/libc-test"
readonly LIBC_TEST_BRANCH="master"
readonly LLVM_TEST_SUITE_URL="https://github.com/llvm/llvm-test-suite"
readonly LLVM_TEST_SUITE_BRANCH="release/13.x"
readonly CSMITH_URL="https://github.com/csmith-project/csmith"
readonly CSMITH_BRANCH="master"

# Configuration variables.
readonly WORK_PATH="${CI_PROJECT_DIR}/work"
readonly WORK_BUILD_PATH="${WORK_PATH}/build"
readonly WORK_INSTALL_PATH="${WORK_PATH}/install"
readonly WORK_TEST_PATH="${WORK_PATH}/test"
readonly BUILD_MORELLO_SCRIPT_PATH="${MUSL_LIBC_PATH}/tools/build-morello.sh"
readonly GITLAB_CI_CHERISEED_PATH="${CI_PROJECT_DIR}/.gitlab-ci/cheriseed"
readonly XZ_FLAGS="-T0 -7"
readonly MANIFEST_PATH="manifest.txt"

# Specification of build directories.
readonly LLVM_PROJECT_BUILD_PATH="${WORK_BUILD_PATH}/llvm-project"

# Specification of test directories.
readonly MUSL_LIBC_TEST_PATH="${WORK_TEST_PATH}/musl-libc"
readonly LIBC_TEST_TEST_PATH="${WORK_TEST_PATH}/libc-test"
readonly LLVM_TEST_SUITE_TEST_PATH="${WORK_TEST_PATH}/llvm-test-suite"
readonly CSMITH_TEST_PATH="${WORK_TEST_PATH}/csmith"

# Remove artifacts from previous stage.
rm -rf "${CI_PROJECT_DIR}"/*.tar.xz

# Build CHERIseed toolchain.
# shellcheck source=build-toolchain-cheriseed.sh
. "${CI_PROJECT_DIR}"/.gitlab-ci/build/build-toolchain-cheriseed.sh

# Run all tests.
test_failed=0

# Run static-built musl-libc tests.
"${GITLAB_CI_CHERISEED_PATH}/utils/musl_libc_tests.py" \
    --enable-cheriseed \
    --timeout=15 \
    --musl-libc-sysroot-path="${MUSL_LIBC_INSTALL_PATH}" \
    --compiler-bin-path="${LLVM_PROJECT_INSTALL_PATH}/bin" \
    --musl-libc-path="${MUSL_LIBC_PATH}" \
    --expected-failures="${GITLAB_CI_CHERISEED_PATH}/expected_failures/musl_libc_test.json" \
    --output-path="${MUSL_LIBC_TEST_PATH}/static" || test_failed=1;

# Run dynamic-built musl-libc tests.
"${GITLAB_CI_CHERISEED_PATH}/utils/musl_libc_tests.py" \
    --enable-cheriseed \
    --dynamic \
    --timeout=15 \
    --musl-libc-sysroot-path="${MUSL_LIBC_INSTALL_PATH}" \
    --compiler-bin-path="${LLVM_PROJECT_INSTALL_PATH}/bin" \
    --musl-libc-path="${MUSL_LIBC_PATH}" \
    --expected-failures="${GITLAB_CI_CHERISEED_PATH}/expected_failures/musl_libc_test.json" \
    --output-path="${MUSL_LIBC_TEST_PATH}/dynamic" || test_failed=1;

clone_repository "${LIBC_TEST_URL}" "${LIBC_TEST_BRANCH}" "${LIBC_TEST_PATH}" "--depth=1"

# Run static-built libc-test suite.
"${GITLAB_CI_CHERISEED_PATH}/utils/libc_tests.py" \
    --enable-cheriseed \
    --libc-test-path="${LIBC_TEST_PATH}" \
    --makefile-path="${GITLAB_CI_CHERISEED_PATH}/libc-test/Makefile" \
    --musl-libc-sysroot-path="${MUSL_LIBC_INSTALL_PATH}" \
    --compiler-bin-path="${LLVM_PROJECT_INSTALL_PATH}/bin" \
    --expected-failures="${GITLAB_CI_CHERISEED_PATH}/expected_failures/libc_tests.json" \
    --output-path="${LIBC_TEST_TEST_PATH}/static" || test_failed=1;

# Run dynamic-built libc-test suite.
"${GITLAB_CI_CHERISEED_PATH}/utils/libc_tests.py" \
    --enable-cheriseed \
    --dynamic \
    --libc-test-path="${LIBC_TEST_PATH}" \
    --makefile-path="${GITLAB_CI_CHERISEED_PATH}/libc-test/Makefile" \
    --musl-libc-sysroot-path="${MUSL_LIBC_INSTALL_PATH}" \
    --compiler-bin-path="${LLVM_PROJECT_INSTALL_PATH}/bin" \
    --expected-failures="${GITLAB_CI_CHERISEED_PATH}/expected_failures/libc_tests.json" \
    --output-path="${LIBC_TEST_TEST_PATH}/dynamic" || test_failed=1;

clone_repository "${LLVM_TEST_SUITE_URL}" "${LLVM_TEST_SUITE_BRANCH}" "${LLVM_TEST_SUITE_PATH}"

# Run static-built llvm-test-suite.
"${GITLAB_CI_CHERISEED_PATH}/utils/llvm_test_suite.py" \
    --llvm-test-suite-path="${LLVM_TEST_SUITE_PATH}" \
    --cache-template-path="${GITLAB_CI_CHERISEED_PATH}/llvm-test-suite/O3-cheriseed-static.template.cmake" \
    --compiler-bin-path="${LLVM_PROJECT_INSTALL_PATH}/bin" \
    --llvm-lit-path="${LLVM_PROJECT_PATH}/llvm/utils/lit/lit.py" \
    --musl-libc-sysroot-path="${MUSL_LIBC_INSTALL_PATH}" \
    --expected-failures="${GITLAB_CI_CHERISEED_PATH}/expected_failures/llvm_test_suite.json" \
    --output-path="${LLVM_TEST_SUITE_TEST_PATH}/static" || test_failed=1;

# Run dynamic-built llvm-test-suite.
"${GITLAB_CI_CHERISEED_PATH}/utils/llvm_test_suite.py" \
    --llvm-test-suite-path="${LLVM_TEST_SUITE_PATH}" \
    --cache-template-path="${GITLAB_CI_CHERISEED_PATH}/llvm-test-suite/O3-cheriseed-dynamic.template.cmake" \
    --compiler-bin-path="${LLVM_PROJECT_INSTALL_PATH}/bin" \
    --llvm-lit-path="${LLVM_PROJECT_PATH}/llvm/utils/lit/lit.py" \
    --musl-libc-sysroot-path="${MUSL_LIBC_INSTALL_PATH}" \
    --expected-failures="${GITLAB_CI_CHERISEED_PATH}/expected_failures/llvm_test_suite.json" \
    --output-path="${LLVM_TEST_SUITE_TEST_PATH}/dynamic" || test_failed=1;

clone_repository "${CSMITH_URL}" "${CSMITH_BRANCH}" "${CSMITH_PATH}" "--depth=1"

# Run csmith generating C files and static build.
"${GITLAB_CI_CHERISEED_PATH}/utils/csmith.py" \
    --iterations=10 \
    --timeout=30 \
    --csmith-path="${CSMITH_PATH}" \
    --reference-compiler-path=/usr/lib/llvm-10/bin/clang \
    --reference-compiler-flags="-O2 -static" \
    --experimental-compiler-path="${LLVM_PROJECT_INSTALL_PATH}/bin/clang" \
    --experimental-compiler-flags="--sysroot=${MUSL_LIBC_INSTALL_PATH} -fuse-ld=lld --rtlib=compiler-rt -fsanitize=cheriseed -mabi=purecap -O2 -static" \
    --output-path="${CSMITH_TEST_PATH}/static/c" || test_failed=1;

# Save manifest information.
{
    echo "CI_JOB_ID: ${CI_JOB_ID+x}"
    echo "LLVM_PROJECT_REVISION: $(get_revision "${LLVM_PROJECT_PATH}")"
    echo "MUSL_LIBC_REVISION: $(get_revision "${MUSL_LIBC_PATH}")"
    echo "LIBSHIM_REVISION: $(get_revision "${LIBSHIM_PATH}")"
    echo "LIBC_TEST_REVISION: $(get_revision "${LIBC_TEST_PATH}")"
    echo "LLVM_TEST_SUITE_REVISION: $(get_revision "${LLVM_TEST_SUITE_PATH}")"
    echo "CSMITH_REVISION: $(get_revision "${CSMITH_PATH}")"
    echo "CLANG_VERSION: ${CLANG_VERSION}"
} > "${MANIFEST_PATH}"

exit "${test_failed}"
