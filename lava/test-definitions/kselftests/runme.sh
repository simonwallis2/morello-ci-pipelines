#!/bin/sh

set +x

mkdir boot
mount /dev/sda1 boot/
cd boot/kselftest_install
./run_kselftest.sh | tee log.txt

echo "<LAVA_SIGNAL_STARTTC kselftest>"
for test in $(cat log.txt | grep "selftests:" | grep "^ok" | awk '{print $NF}'); do
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=pass>"
done
for test in $(cat log.txt | grep "selftests:" | grep "^not ok" | awk '{print $NF}'); do
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=fail>"
done
echo "<LAVA_SIGNAL_ENDTC kselftest>"
