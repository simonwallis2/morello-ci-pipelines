#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo userdata.tar.xz "${USERDATA_URL}"
tar xf userdata.tar.xz

test="compartment-demo"
adb push "data/nativetest64/${test}/${test}" \
  "/data/nativetest64/${test}/${test}"
adb push "data/nativetest64/${test}/compartments/" \
  "/data/nativetest64/${test}/compartments/"
adb shell "/data/nativetest64/${test}/${test}" 2>&1 \
  | tee /tmp/results.log

grep -q "compartment demo completed" /tmp/results.log
status=$?
echo "<LAVA_SIGNAL_STARTTC ${test}>"
if [ "$status" -eq 0 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC ${test}>"
rm -f /tmp/results.log
