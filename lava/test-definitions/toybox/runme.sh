#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo toybox-testfiles.tar.xz "${TOYBOX_URL}"
tar -xf toybox-testfiles.tar.xz

# The tests are running on the host and connecting to the target during the run
cd toybox-testfiles

for toy in tests/*.test; do
  test=`echo $toy | sed 's|tests/||' | sed 's|\.test||'`
  ./run-tests-on-android.sh ${test} 2>&1 | tee /tmp/toybox-${test}-log.txt

  grep -v "(ignoring)" /tmp/toybox-log.txt | grep -q "FAILED"

  status=$?
  echo "<LAVA_SIGNAL_STARTTC ${test}>"
  if [ "$status" -eq 1 ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=fail>"
  fi
  echo "<LAVA_SIGNAL_ENDTC ${test}>"
  rm -f /tmp/toybox-${test}-log.txt
done
