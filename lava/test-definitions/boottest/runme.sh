#!/bin/sh

set +x

BOOT_TIMEOUT="7200"

apt update -q=2
apt install -q=2 --yes --no-install-recommends openimageio-tools

# Let's sleep for 20 minutes as adbd is expected to restart.
if [[ ! -z "${PLATFORM+x}" && "${PLATFORM}" == "fvp" ]]; then
    REFERENCE_PNG=reference-screen-fvp.png
    sleep 1200
else
    REFERENCE_PNG=reference-screen-soc.png
    sleep 180
fi

# Let's try adb connection here again
iter=180
interval="10s"

while [ ${iter} -ne 0 ]; do
  adb connect "${ANDROID_SERIAL}"
  adb get-state && status=0 || status=$?
  if [ "${status}" -eq 0 ]; then
    adb devices
    break
  fi
  sleep ${interval}
  iter=$(( iter - 1 ))
done

boot_completed=false
for i in {1..120}; do
  if adb shell getprop sys.boot_completed | grep "1"; then
    boot_completed=true
    echo "Got sys.boot_completed property"
    break
  else
    sleep 60
  fi
done

if $boot_completed; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=boot RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=boot RESULT=fail>"
fi

if [[ ! -z "${PLATFORM+x}" && "${PLATFORM}" == "fvp" ]]; then
    sleep 3000
fi


for i in $(seq 1 6); do
time adb shell screencap -p > /tmp/screen.png
idiff -failpercent 1 -warn 0.004 -warnpercent 3 /tmp/screen.png ${TEST_DIR}/${REFERENCE_PNG}
  if [ $? -eq 0 ]; then
	echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=ui-boot-screen RESULT=pass>"
	break
  fi
  if [ "$i" -lt 6 ]; then
	echo "Screen did not match. Going to sleep for 5 for minutes..."
	sleep 300
  else
	echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=ui-boot-screen RESULT=fail>"
	break
  fi
done
